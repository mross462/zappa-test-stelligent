#!/bin/bash
if ! which aws > /dev/null; then
        echo GO INSALL AWS CLI NOW https://docs.aws.amazon.com/cli/latest/userguide/installing.html!
fi
source `pwd`/.aws_credentials
PROJECT_NAME=`basename "$PWD"`
ACCOUNT_ID=`aws sts get-caller-identity --output text --query 'Account'`
aws iam detach-role-policy --role-name ZappaLambdaExecution --policy-arn "arn:aws:iam::$ACCOUNT_ID:policy/ZappaExecutionPolicy"
aws iam delete-role --role-name ZappaLambdaExecution
aws iam delete-user-policy --user-name $PROJECT_NAME --policy-name $PROJECT_NAME-s3-createbucketpolicy 
aws iam delete-user-policy --user-name $PROJECT_NAME --policy-name $PROJECT_NAME
aws iam delete-access-key --user-name $PROJECT_NAME --access-key-id $AWS_ACCESS_KEY_ID
aws iam delete-user --user-name $PROJECT_NAME
aws s3 rb s3://$PROJECT_NAME
