from flask import Flask
app = Flask(__name__)

@app.route('/')
def index():
    return ("Hello, Stelligent!", 200)

@app.route('/error')
def error():
    raise ValueError('Error Test')
    return

# We only need this for local development.
if __name__ == '__main__':
    app.run()