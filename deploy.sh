#!/bin/bash
source .aws_credentials
BRANCH=`git symbolic-ref --short HEAD`
#Make sure we always have the most updated exeuciton policy
aws iam put-role-policy --role-name ZappaLambdaExecution --policy-name ZappaExecutionPolicy --policy-document file://iam/zappa_lambda_execution.json
docker run -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
                -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
                -e AWS_DEFAULT_REGION=us-west-2 \
                -e BRANCH=$BRANCH\
                -v $(pwd):/var/task \
                --rm mcrowson/zappa-builder bash -c "echo Branch: $BRANCH; virtualenv docker_env && source docker_env/bin/activate && pip install --upgrade pip;  pip install -r requirements.txt && zappa update $BRANCH|| zappa deploy $BRANCH && rm -rf docker_env"