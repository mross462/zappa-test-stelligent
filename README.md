Put simply:
On a machine with docker and the aws cli installed and configured for a user able to create an iam user
```bash
git clone git@bitbucket.org:mross462/zappa-test-stelligent 
cd zappa-test-stelligent
./setup.sh
./deploy.sh
```

To Stop
```bash
./terminate.sh
```