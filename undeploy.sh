#!/bin/bash
source .aws_credentials
BRANCH=`git symbolic-ref --short HEAD`
docker run -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
                -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
                -e AWS_DEFAULT_REGION=us-west-2 \
                -e BRANCH=$BRANCH\
                -v $(pwd):/var/task \
                --rm mcrowson/zappa-builder bash -c "virtualenv docker_env && source docker_env/bin/activate && pip install -r requirements.txt && zappa undeploy $BRANCH -y && rm -rf docker_env"