#!/bin/bash
set -e
if ! which aws > /dev/null; then
        echo GO INSALL AWS CLI NOW https://docs.aws.amazon.com/cli/latest/userguide/installing.html!
fi
PROJECT_NAME=`basename "$PWD"`
aws iam create-user --user-name $PROJECT_NAME
KEY_OUTPUT=`aws iam create-access-key --user-name $PROJECT_NAME`
AWS_ACCESS_KEY_ID=`echo $KEY_OUTPUT | jq '.AccessKey.AccessKeyId'`
AWS_SECRET_ACCESS_KEY=`echo $KEY_OUTPUT | jq '.AccessKey.SecretAccessKey'`
echo AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID > .aws_credentials
echo AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY >> .aws_credentials
source .aws_credentials

#Perform Necessary IAM Tasks
aws iam put-user-policy --user-name $PROJECT_NAME \
                        --policy-name $PROJECT_NAME-s3-createbucketpolicy \
                        --policy-document file://`pwd`/iam/s3_policy.json
ACCOUNT_ID=`aws sts get-caller-identity --output text --query 'Account'`
sed -ie "s/BUCKET/$PROJECT_NAME/g" `pwd`/iam/iam_policy.json
sed -ie "s/ACCOUNT_ID/$ACCOUNT_ID/g" `pwd`/iam/iam_policy.json

#Create IAM Role and Policy if not fall through
aws iam create-role --role-name ZappaLambdaExecution \
                    --assume-role-policy-document file://`pwd`/iam/trust_relationship.json  || true
aws iam create-policy --policy-name ZappaExecutionPolicy \
                      --policy-document file://iam/zappa_lambda_execution.json | jq -r '.Policy.Arn' || true
#Update the policy
aws iam put-user-policy --user-name $PROJECT_NAME \
                        --policy-name $PROJECT_NAME \
                        --policy-document file://`pwd`/iam/iam_policy.json

aws iam attach-role-policy --role-name ZappaLambdaExecution --policy-arn "arn:aws:iam::$ACCOUNT_ID:policy/ZappaExecutionPolicy"

#Create the s3 bucket for the code
aws s3 mb s3://$PROJECT_NAME
